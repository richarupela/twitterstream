# TweetAPI

# Features
- API to query data from Twitter based on search term
- Data must be filterable based on any key in the search data.
- Complex filters dependent on data-type needs to be implemented. For example,
      - Date range filter in case of date. example created_at__gte, created_at, created_at__lte
      - Less than, greater than, equal to filter in case of integer. example : retweet_count__gte, retweet_count, retweet_count__lte
      - Starts with, ends with, contains, exact match in case of string. example queries supported : text__startswith, text__endswith, text__contains, text__exact and text 
- Implement sort option. You can sort by any field in ascending or descending order.

## Projet Setup
```
# create a virtualenv 
virtualenv tweetstream_env
source tweetstream_env/bin/activate
pip install -r reuqirements.txt
./manage.py migrate
./manage.py runserver
```
 

## Fetch tweets from twitter

```
python manage.py fetchtweets `query_term`
```
example : 
```
     python manage.py fetchtweets cat
```

## API endpoint for Tweets List View
```
http://localhost:8000/apiv1/tweets/
```

### Date time based filters


```
http://localhost:8000/apiv1/tweets/?created_at__start=2017-11-10T19:24:56Z
http://localhost:8000/apiv1/tweets/?created_at__end=2017-11-10T19:24:56Z
http://localhost:8000/apiv1/tweets/?created_at__start=2017-11-10T19:24:56Z&created_at__end=2017-11-10T19:24:57Z
```


### Text Filtering

```
http://localhost:8000/apiv1/tweets/?text__contains='python'
http://localhost:8000/apiv1/tweets/?text__exact='python'
http://localhost:8000/apiv1/tweets/?text__startswith='python'
http://localhost:8000/apiv1/tweets/?text__endswith='python'
```

### Integer Filtering

```
http://localhost:8000/apiv1/tweets/?retweet_count=0
http://localhost:8000/apiv1/tweets/?retweet_count__lte=5
http://localhost:8000/apiv1/tweets/?retweet_count__gte=3
```
same for favorite_count

### Ordering
ordering in ascending/descending of field

```
http://localhost:8000/apiv1/tweets/?orderby=retweet_count
http://localhost:8000/apiv1/tweets/?orderby=-retweet_count
```