from rest_framework import serializers

from .models import Tweets


class TwitterSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tweets
        fields = ('favorite_count', 'text', 'created_at', 'tweet_id', 'retweet_count')
