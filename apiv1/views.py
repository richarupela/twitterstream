from django.db import models

from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import TwitterSerializer
from .models import Tweets


class OrderByMixin(object):

    """
    Ordering Mixin to sort by any field from the model.
    """

    def get_queryset(self, *args, **kwargs):
        queryset = Tweets.objects.all()
        order_by = self.request.query_params.get('orderby')
        field_name = order_by
        if order_by:
            if order_by.startswith('-'):
                field_name = order_by[1:]
            if field_name not in [field.attname for field in Tweets._meta.fields]:
                raise ValueError('Invalid Field entered for ordering.')
            queryset = queryset.order_by(order_by)
        return queryset


class TwitterAPI(OrderByMixin, APIView):

    """
    API endpoint to filter tweets with Integer Filtering
    String search filter and Date range filter.
    """
    queryset = Tweets.objects.all()

    @staticmethod
    def search_options(field_name):
        return [
            "{}__gte".format(field_name),
            "{}__lte".format(field_name),
            "{}".format(field_name),
        ]

    def get_queryset(self, *args, **kwargs):
        qs = super(TwitterAPI, self).get_queryset(*args, **kwargs)
        return qs

    def get(self, request, format=None):
        tweets = self.get_queryset()

        text_contains = request.query_params.get('text__contains')
        text_exact = request.query_params.get('text__exact')
        text_starts = request.query_params.get('text__startswith')
        text_ends = request.query_params.get('text__endswith')
        end_date = request.query_params.get('created_at__end')
        start_date = request.query_params.get('created_at__start')

        # Text based filtering for the string fields present.
        if text_contains:
            tweets = tweets.filter(text__icontains=text_contains)
        if text_exact:
            tweets = tweets.filter(text__exact=text_exact)
        if text_starts:
            tweets = tweets.filter(text__startswith=text_starts)
        if text_ends:
            tweets = tweets.filter(text__endswith=text_ends)
        # Date Range Filtering.
        if start_date:
            tweets = tweets.filter(created_at__gte=start_date)
        if end_date:
            tweets = tweets.filter(created_at__lte=end_date)

        # Integer based filtering for all the integer fields present.
        integer_fields = ['retweet_count', 'favorite_count']
        for field_name in integer_fields:
            search_options = self.search_options(field_name)
            for field in search_options:
                tweet_field = request.query_params.get(field)
                if tweet_field:
                    print field, tweet_field
                    query = models.Q(**{field: tweet_field})
                    tweets = tweets.filter(query)
        serializer = TwitterSerializer(tweets, many=True)
        return Response(serializer.data)
