from __future__ import unicode_literals

from django.db import models


class Tweets(models.Model):
    favorite_count = models.IntegerField(default=0)
    text = models.TextField()
    created_at = models.DateTimeField()
    tweet_id = models.IntegerField()
    retweet_count = models.IntegerField()
    location = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        verbose_name = 'tweet'
        verbose_name_plural = 'tweets'
