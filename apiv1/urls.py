from django.conf.urls import url

from .views import TwitterAPI

urlpatterns = [
    url(r'^tweets', TwitterAPI.as_view(), name='twitter_api'),
]
